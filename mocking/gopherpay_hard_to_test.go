package mocking

import (
	"github.com/go-redis/redis"
)

// GopherpayNode redirects traffic
type GopherpayNodeBad struct {
	redisClient *redis.Client
}

// MyHandler is our demo handler.
func (node *GopherpayNodeBad) MyHandler() {
	node.redisClient.Incr("foo")
}
