package mocking

import (
	"testing"

	"github.com/go-redis/redis"
)

func TestGopherpayNodeMyHandler(t *testing.T) {

	mockRedis := &MockRedis{}

	returnValue := (*redis.IntCmd)(nil)

	mockRedis.On("Incr", "foo").Once().Return(returnValue)

	node := &GopherpayNode{
		redisClient: mockRedis}

	node.MyHandler()

	mockRedis.AssertExpectations(t)
}
