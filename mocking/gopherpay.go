package mocking

// GopherpayNode redirects traffic
type GopherpayNode struct {
	redisClient RedisInterface
}

// MyHandler is our demo handler.
func (node *GopherpayNode) MyHandler() {
	node.redisClient.Incr("foo")
}
