package mocking

import (
	"github.com/go-redis/redis"
	"github.com/stretchr/testify/mock"
)

// RedisInterface is the shared interface between redis.Client and MockRedis
type RedisInterface interface {
	Incr(key string) *redis.IntCmd
}

// Trick to guarantee both satisfy the interface
var _ RedisInterface = (*MockRedis)(nil)
var _ RedisInterface = (*redis.Client)(nil)

// MockRedis is a mocked redis
type MockRedis struct {
	mock.Mock
}

// Incr is a mock of the redis Incr command
func (mockRedis *MockRedis) Incr(key string) *redis.IntCmd {
	args := mockRedis.Called(key)
	return args.Get(0).(*redis.IntCmd)
}
