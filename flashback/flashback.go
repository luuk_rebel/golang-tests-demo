package main

import (
	"fmt"
	"math"
)

// Shape is a shape
type Shape interface {
	Area() float64
}

// Square is a shape with four equal sides
type Square struct {
	Side float64
}

// Area computes the area of a Square
func (square Square) Area() float64 {
	return square.Side * square.Side
}

// Circle is a round shape
type Circle struct {
	Radius float64
}

// Area computes the area of a Circle
func (circle Circle) Area() float64 {
	return math.Pi * circle.Radius * circle.Radius
}

// Person is not a shape
type Person struct {
	Name string
}

// check which types are a Shape
var _ Shape = (*Square)(nil)
var _ Shape = (*Circle)(nil)
var _ Shape = (*Person)(nil)

func main() {

	shapes := []Shape{
		Circle{Radius: 2.0},
		Square{Side: 2.0}}

	totalArea := 0.0
	for _, shape := range shapes {
		totalArea += shape.Area()
	}

	fmt.Printf("totalArea = %3.2f\n\n", totalArea)
}
