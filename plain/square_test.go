package plain

import "testing"

func TestSquare(t *testing.T) {

	type testCase struct {
		input          int
		expectedResult int
	}

	testCases := []testCase{
		{0, 0},
		{1, 1},
		{2, 4},
		{30, 900},
		{-5, 25},
		{10, 100}}

	for _, testCase := range testCases {
		got := Square(testCase.input)
		if testCase.expectedResult != got {
			t.Errorf("For Square(%d), expected %d but got %d",
				testCase.input, testCase.expectedResult, got)
		}
	}

}
