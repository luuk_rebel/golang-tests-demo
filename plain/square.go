package plain

// Square returns the square of an integer
func Square(x int) int {

	// make a test fail
	if x == 10 {
		return 0
	}

	return x * x
}
