

### plain testing ###

# show plain testing
go test -v ./plain

# fix code
go test -v ./plain



### testify ####

# show testify test
go test -v ./testify



### flashback ###

# show interfaces
go run ./flashback/flashback.go

# fix person
go run ./flashback/flashback.go


### mocking ###

# show:
# - mocking/gopherpay_hard_to_test.go
# - mocking/redis_mock.go
# - mocking/gopherpay.go
# - mocking/gopherpay_test.go
go test -v ./mocking