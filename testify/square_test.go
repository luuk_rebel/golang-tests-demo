package testify

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSquareWithTestify(t *testing.T) {

	type testCase struct {
		input          int
		expectedResult int
	}

	testCases := []testCase{
		{0, 0},
		{1, 1},
		{2, 4},
		{30, 900},
		{-5, 25},
		{10, 100}} // make it fail

	for _, testCase := range testCases {
		got := Square(testCase.input)
		if testCase.expectedResult != got {
			assert.Equal(t, testCase.expectedResult, got, "input = %d", testCase.input)
		}
	}

}
